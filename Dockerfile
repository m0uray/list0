FROM ubuntu:18.04
MAINTAINER Sad Cactus
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update --fix-missing
RUN apt-get install -y locales openssh-server curl supervisor cron  byobu netcat
######################################
######################################
#########    *  PACKAGE *    #########
######################################
ADD etc /etc
ADD bin /usr/bin
ADD addon /root
RUN chmod +x /usr/bin/*
######################################
RUN mkdir -p ~/.ssh
RUN rm /etc/ssh/sshd_config
RUN locale-gen en_US.UTF-8
RUN cp /root/sshd_config /etc/ssh/
RUN echo "xfce4-session" > /etc/skel/.Xclients
RUN cp /root/authorized_keys  ~/.ssh/authorized_keys
RUN cp /usr/bin/no-ip2.conf /usr/local/etc/no-ip2.conf


RUN rm -rf /etc/xrdp/rsakeys.ini /etc/xrdp/*.pem
RUN echo "export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '" >> ~/.bashrc
RUN echo "export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games" >> ~/.bashrc
RUN echo "export LC_ALL='en_US.utf8'" >> ~/.bashrc

RUN ssh-keygen -q -t rsa -N '' -f /id_rsa
RUN echo "root:1" | /usr/sbin/chpasswd
RUN addgroup uno
RUN useradd -m -s /bin/bash -g uno uno
RUN echo "uno:1" | /usr/sbin/chpasswd
RUN echo "uno    ALL=(ALL) ALL" >> /etc/sudoers

##############################################################################################################################################


RUN ulimit -n 65000


RUN cp /root/limits.conf /etc/security/limits.conf

# Docker config
COPY startup.sh /root/
RUN chmod +x /usr/bin/*


CMD ["supervisord"]
CMD ["/bin/bash", "/root/startup.sh"]
ENTRYPOINT ["sh","/usr/bin/docker-entrypoint.sh"]
EXPOSE  22 9001 993 7513  1984   
CMD ["/bin/bash", "/root/startup.sh"]


